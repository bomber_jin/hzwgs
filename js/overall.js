/**
 * Created by SEELE on 2017/11/27.
 */
//header
let user_name = '';
let user_id='';
$.ajax({
    url: '/bmadmin/users/getNowLoginUser',
    async: false,
    type: 'post',
    success: function (ret) {
        ret = JSON.parse(ret);
        if (ret.code == '0000') {
            user_name=ret.object.u_name;
            user_id=ret.object.u_id;
        }
        else {
            top.location.href = "/login.html";
        }
    },
    error: function () {
        layer.msg('登陆失败，请确认用户名和密码');
    }
});

let header_data = `<div class="navbar navbar-fixed-top">
            <div class="container-fluid cl"><a class="logo navbar-logo f-l mr-10 hidden-xs" href="/index.html"><img src="../img/m-logo.png" style="width: 40px;"> 杭州湾新区税务局一体化平台</a> <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/aboutHui.shtml"></a>
                <span class="logo navbar-slogan f-l mr-10 hidden-xs"></span>
                <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>

                <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                    <ul class="cl">
                    <li style="margin-right: 21px;color: white">${user_name}</li>
                    <li class="header-repsd" style="cursor:pointer;height: 21px;margin-top: 18px;margin-right: 20px;width: 21px;" onclick="resetPassword()"></li>
                    <li class="header-quite" style="cursor:pointer;width: 19px;height: 20px;margin-top: 18px" onclick="safeQuit()"></li>
                   
                    </ul>
                </nav>
            </div>
        </div>
        <div id="modal-reset-password" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content radius">
			<div class="modal-header">
				<h3 class="modal-title">密码修改</h3>
				<a class="close" data-dismiss="modal" aria-hidden="true" href="javascript:void();">×</a>
			</div>
			<div class="modal-body">
				<div><span style="font-size: 16px">新密码</span><input type="password" class="input-text radius" id="newPasswordOne" style="margin-top: 10px"></div>
				<div><span style="font-size: 16px">确认密码</span><input type="password" class="input-text radius" id="newPasswordTwo" style="margin-top: 10px"></div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="resetPasswordCommit('${user_id}')">确定</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
			</div>
		</div>
	</div>
</div>
`;
$(".navbar-wrapper").append(header_data);

//aside
/*let aside_data =`<div class="menu_dropdown bk_2">`;

let userListInfo=JSON.parse(sessionStorage.getItem('userListInfo'));

aside_data += `<dl ><dt><i class="Hui-iconfont">&#xe625;</i> <a href="/index.html">我的主页</a> </dt></dl>`;
userListInfo.forEach(function (value,index,array) {

    aside_data += `<dl id="${value.p_id}"><dt><i class="Hui-iconfont">&#${value.p_icon};</i> ${value.p_name}<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>`;
    aside_data += `<dd><ul>`;

    let childrenList = value.children;

    childrenList.forEach(function (childrenValue,index,array) {
        aside_data += `<li><a href="${childrenValue.p_url}" id="${childrenValue.p_id}" title="">${childrenValue.p_name}</a></li>`;
    });

    aside_data += `</ul></dd></dl>`;
});*/

let aside_data =`<div id="side-nav"><ul id="nav">`;

let userListInfo=JSON.parse(sessionStorage.getItem('userListInfo'));

aside_data += `<li ><a href="javascript:;" _href="main.html"><i class="Hui-iconfont">&#xe625;</i> <cite>我的主页</cite></a></li>`;
userListInfo.forEach(function (value,index,array) {

    aside_data += `<li id="${value.p_id}"><a><i class="Hui-iconfont">&#${value.p_icon};</i> <cite>${value.p_name}</cite><i class="iconfont nav_right">&#xe615;</i></a>`;
    aside_data += `<ul class="sub-menu">`;

    let childrenList = value.children;

    childrenList.forEach(function (childrenValue,index,array) {
        aside_data += `<li><a _href="${childrenValue.p_url}" id="${childrenValue.p_id}" title=""> <i class="iconfont">&#xe666;</i><cite>${childrenValue.p_name}</cite></a></li>`;
    });

    aside_data += `</ul></li>`;
});


aside_data += '</ul></div>';

$(".left-nav").append(aside_data);
aside_data += '</div>';

$(".Hui-aside").append(aside_data);

//footer
let footer_data = `
       <!--<p class="text-c">-->
        <!--<span >  <a href="#">国税是树，基层是根，管理是本，文化是魂</a> </span></p>-->
    `;
$(".layui-footer").append(footer_data);

//用户退出
function safeQuit() {

    layer.msg('确定退出用户？', {
        time: 0 //不自动关闭
        , btn: ['确定', '取消']
        , yes: function () {
            $.ajax({
                url: '/bmadmin/users/safeQuit',
                async: false,
                type: 'post',
                success: function (ret) {
                    top.location.href = "/login.html";

                    sessionStorage.removeItem("menu")

                },
                error: function () {
                    layui.use('layer', function(){
                        var layer = layui.layer;
                        layer.msg("系统操作失败，请联系系统管理员");
                    });
                }
            });
        }
    });






}

//用户修改密码-显示框
function resetPassword() {
    $("#modal-reset-password").modal("show");
}
//用户修改密码-提交
function resetPasswordCommit(u_id) {
    let psdOne=$('#newPasswordOne').val();
    let psdTwo=$('#newPasswordTwo').val();

    if(psdOne&&psdTwo){
        if(psdOne!=psdTwo){
            layui.use('layer', function(){
                var layer = layui.layer;
                layer.msg("两次输入密码不同，请重新输入");
            });
            return;
        }
        var reg = /((^(?=.*[a-z])(?=.*[A-Z])(?=.*\W)[\da-zA-Z\W]{8,16}$)|(^(?=.*\d)(?=.*[A-Z])(?=.*\W)[\da-zA-Z\W]{8,16}$)|(^(?=.*\d)(?=.*[a-z])(?=.*\W)[\da-zA-Z\W]{8,16}$)|(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\da-zA-Z\W]{8,16}$))/;
        if (!reg.test(psdOne)) {
            layer.alert("请输入8-16位字符，至少包含数字、大写字母、小写字母、特殊字符中的三种类型", {
                title: '提示信息'
            })
            return;
        }
    }else{
        layui.use('layer', function(){
            var layer = layui.layer;
            layer.msg("不得输入空密码");
        });
        return;
    }
    $.ajax({
        url: '/bmadmin/users/resetPassword',
        async: false,
        type: 'post',
        data: {'u_id': u_id, "u_password": psdOne},
        success: function (ret) {
            ret = JSON.parse(ret);
            if (ret.code == '0000') {
                layui.use('layer', function(){
                    var layer = layui.layer;
                    layer.msg(ret.msg+'两秒后请重新登录');
                });
                setInterval(function () {
                    top.location.href = "/login.html";
                },2000);

            }
            else {
                layui.use('layer', function(){
                    var layer = layui.layer;

                    layer.msg(ret.msg);
                });
            }
        },
        error: function () {
            layui.use('layer', function(){
                var layer = layui.layer;

                layer.msg('登陆失败，请确认用户名和密码');
            });
        }
    });

}